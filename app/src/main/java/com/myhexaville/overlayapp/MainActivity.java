package com.myhexaville.overlayapp;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = "MainActivity";
    public static final int RC_OVERLAY_PERMISSION = 1;

    public static Context context;

    public SeekBar m_seekBar;
    public TextView m_seekBar_Value_text;
    public Switch m_Switch;
    public  float m_NoiseValue=50f;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context=this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        m_Switch=(Switch)findViewById(R.id.switch7);
        m_seekBar=(SeekBar) findViewById(R.id.seekBar6);
        m_seekBar_Value_text=(TextView)findViewById(R.id.seekValue);


           // m_seekBar.setProgress((int)m_NoiseValue);
            m_seekBar_Value_text.setText(m_seekBar.getProgress() + " ");

            m_seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    m_seekBar_Value_text.setText(progress +" ");
                    m_NoiseValue=progress * .005f;

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    NoiseEffect.setNoiseIntensity(m_NoiseValue );
                    startService(new Intent(context, NoiseService.class).putExtra("size", 5000));
                }
            });



            if (!Settings.canDrawOverlays(this)) {
                Intent myIntent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                myIntent.setData(Uri.parse("package:" + getPackageName()));
                startActivityForResult(myIntent, RC_OVERLAY_PERMISSION);

                NoiseEffect.setNoiseIntensity(m_NoiseValue);
                startService(new Intent(context, NoiseService.class).putExtra("size", 5000));
            } else {
                m_Switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        // do something, the isChecked will be
                        // true if the switch is in the On position
                        if(isChecked){
                            m_seekBar.setEnabled(true);
                            m_seekBar.setProgress(50);
                            m_seekBar_Value_text.setText(m_seekBar.getProgress() +"");
                            NoiseEffect.setNoiseIntensity(m_NoiseValue );
                            startService(new Intent(context, NoiseService.class).putExtra("size", 5000));

                        }else{
                            m_seekBar.setEnabled(false);

                            stopService(new Intent(context, NoiseService.class).putExtra("size", 5000));

                        }

                    }
                });

            }
        /// Changing Switch state according to Service State.
        if(isMyServiceRunning(NoiseService.class)){
            m_seekBar.setEnabled(true);
                m_Switch.setChecked(true);
        }else{
            m_seekBar.setEnabled(false);
        m_Switch.setChecked(false);
        }
        ///


    }


    private boolean isMyServiceRunning(Class<?> serviceClass) { //for checking any service running or not. This is needed for the changing switch state according to Service state
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            startService(new Intent(this, NoiseService.class));

        }
    }
}